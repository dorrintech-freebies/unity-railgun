# Unity Railgun

A railgun particle effect made in unity.

## License

See the [License](/LICENSE) for details. In short, you may use this for any purpose, including commercial use.

## Contact

We'd love to hear from you if you use this you can find us here:

- [dorrin.tech](https://dorrin.tech)
- Twitter: [@DorrinTech](https://twitter.com/DorrinTech)
