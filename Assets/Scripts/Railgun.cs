﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Railgun : MonoBehaviour
{
	[SerializeField]
	Projectile projectile;

	public void Fire() {
		Instantiate(projectile, transform.position, transform.rotation);
	}
}
